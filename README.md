# README #

Spring boot service compatible with spring-boot-commons and zeta common libraries for development / deployment. For graphql specification we have used [graphql-spqr](https://github.com/leangen/graphql-spqr)

### What is this repository for? ###

* Quick summary
    * POC for using graphql engine for atlas cluster
* [Proposal](https://docs.google.com/document/d/1T0GHE9LNQewqHDxK1H718Yw3ljY68Tb8UOaAji_EhQU/edit)

### How do I get set up? ###

* GraphQL gui :  ```localhost:8080/graphiql```
  * Sample Query
	```
	query AtlasResource($tenantId: Long!, $businessId: Long!, $storeId: Int!, $counterId: Int!) {
	  issuerBusiness(tenantId : $tenantId, businessId: $businessId) {
		issuerStore(storeId : $storeId) {
		  issuerCounter(counterId: $counterId) {
			ifiToLedgerMap
			issuerTerminal(aid: "AID", mid: "MID", tid: "TID") {
			  aid
			  mid
			  tid
			}
		  }
		}
	  }
	}
	```
    Variables
    ```
    {
    "businessId" : 1,
    "tenantId": 1,
    "storeId": 1,
    "counterId" : 1
    }
    ```

* Configuration
* Dependencies
  * spring-boot-commons
* Database configuration
  * not integrated yet
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* OAK - BU atlas team
* devs - uday sawhney