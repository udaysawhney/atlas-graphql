package com.example.atlasgraphql.service;

import com.example.atlasgraphql.model.IssuerTerminal;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
public class IssuerTerminalService {

    public CompletionStage<IssuerTerminal> getIssuerTerminal(Long businessId, Integer storeId, Integer counterId, String aid, String mid, String tid) {
        return CompletableFuture.supplyAsync(() -> IssuerTerminal.builder().businessId(businessId).storeId(storeId).counterId(counterId).aid(aid).mid(mid).tid(tid).build());
    }
}
