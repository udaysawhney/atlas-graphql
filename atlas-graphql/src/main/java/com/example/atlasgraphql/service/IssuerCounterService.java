package com.example.atlasgraphql.service;

import com.example.atlasgraphql.model.IssuerCounter;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
public class IssuerCounterService {

    public CompletionStage<IssuerCounter> getIssuerCounter(Long businessId, Integer storeId, Integer counterId) {
        return CompletableFuture.supplyAsync(() -> IssuerCounter.builder().storeId(storeId).businessId(businessId).counterId(counterId).ifiToLedgerMap(ImmutableMap.of("1","ledger-id-1", "2", "ledger-id-2")).build());
    }
}
