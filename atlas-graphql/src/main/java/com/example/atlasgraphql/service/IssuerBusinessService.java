package com.example.atlasgraphql.service;

import com.example.atlasgraphql.dao.IssuerBusinessRepository;
import com.example.atlasgraphql.model.IssuerBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletionStage;

@Service
public class IssuerBusinessService {

    private final IssuerBusinessRepository issuerBusinessRepository;
    private final IssuerStoreService issuerStoreService;

    @Autowired
    public IssuerBusinessService(IssuerBusinessRepository issuerBusinessRepository, IssuerStoreService issuerStoreService) {
        this.issuerBusinessRepository = issuerBusinessRepository;
        this.issuerStoreService = issuerStoreService;
    }

    public CompletionStage<IssuerBusiness> getIssuerBusiness(Long businessId, Long tenantId) {
        return issuerBusinessRepository.getIssuerBusiness(businessId, tenantId);
    }

    public CompletionStage<List<Integer>> getStoreIdsForIssuerBusiness(Long businessId, Long tenantId) {
        return issuerStoreService.getStoreIdsForIssuerBusiness(businessId, tenantId);
    }
}
