package com.example.atlasgraphql.service;

import com.example.atlasgraphql.model.IssuerStore;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
public class IssuerStoreService {

    public CompletionStage<List<Integer>> getStoreIdsForIssuerBusiness(Long businessId, Long tenantId) {
        return CompletableFuture.supplyAsync(() -> Arrays.asList(1,2,3,4,5,6));
    }

    public CompletionStage<IssuerStore> getIssuerStore(Long businessId, Integer storeId) {
        return CompletableFuture.supplyAsync(() -> IssuerStore.builder().storeId(storeId).storeName("S-1").businessId(businessId).build());
    }
}
