package com.example.atlasgraphql.query;

import com.example.atlasgraphql.model.IssuerBusiness;
import com.example.atlasgraphql.model.IssuerCounter;
import com.example.atlasgraphql.model.IssuerStore;
import com.example.atlasgraphql.model.IssuerTerminal;
import com.example.atlasgraphql.model.Vector;
import com.example.atlasgraphql.service.IssuerBusinessService;
import com.example.atlasgraphql.service.IssuerCounterService;
import com.example.atlasgraphql.service.IssuerStoreService;
import com.example.atlasgraphql.service.IssuerTerminalService;
import in.zeta.spectra.capture.SpectraLogger;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLQuery;
import olympus.trace.OlympusSpectra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

@Component
public class IssuerBusinessQuery {

    public static final String BUSINESS_ID = "businessId";
    private final SpectraLogger logger = OlympusSpectra.getLogger(IssuerBusinessQuery.class);
    private final IssuerBusinessService issuerBusinessService;
    private final IssuerStoreService issuerStoreService;
    private final IssuerCounterService issuerCounterService;
    private final IssuerTerminalService issuerTerminalService;

    @Autowired
    public IssuerBusinessQuery(IssuerBusinessService issuerBusinessService, IssuerStoreService issuerStoreService, IssuerCounterService issuerCounterService, IssuerTerminalService issuerTerminalService) {
        this.issuerBusinessService = issuerBusinessService;
        this.issuerStoreService = issuerStoreService;
        this.issuerCounterService = issuerCounterService;
        this.issuerTerminalService = issuerTerminalService;
    }


    @GraphQLQuery(name = "issuerBusiness")
    public CompletableFuture<IssuerBusiness> issuerBusiness(@GraphQLArgument(name = BUSINESS_ID) Long businessId, @GraphQLArgument(name = "tenantId") Long tenantId) {
        logger.info("query for issuer business ").attr(BUSINESS_ID, businessId).attr("tenantId", tenantId).log();
        return issuerBusinessService.getIssuerBusiness(businessId, tenantId).toCompletableFuture();
    }

    @GraphQLQuery(name = "storeIds")
    public CompletableFuture<List<Integer>> getStoreIds(@GraphQLContext IssuerBusiness issuerBusiness) {
        logger.info("issuer business query for storeids").attr(BUSINESS_ID, issuerBusiness.getBusinessId()).log();
        return issuerStoreService.getStoreIdsForIssuerBusiness(issuerBusiness.getBusinessId(), issuerBusiness.getTenantId()).toCompletableFuture();
    }

    @GraphQLQuery(name = "vectors")
    public CompletableFuture<List<Vector>> getVectors(@GraphQLContext IssuerBusiness issuerBusiness, @GraphQLArgument(name = "pageNo") Integer pageNo, @GraphQLArgument(name = "pageSize") Integer pageSize) {
        logger.info("issuer business query for vectors").attr(BUSINESS_ID, issuerBusiness.getBusinessId()).attr("pageNo", pageNo).log();
        return CompletableFuture.supplyAsync(() -> Collections.singletonList(
                Vector.builder().vectorName("affiliateId").vectorValue("666").entityName("BUSINESS").entityValue("1").build()
        ));
    }

    @GraphQLQuery(name = "issuerStore")
    public CompletableFuture<IssuerStore> getIssuerStore(@GraphQLContext IssuerBusiness issuerBusiness, @GraphQLArgument(name = "storeId") Integer storeId) {
        logger.info("issuer business query for issuer store").attr(BUSINESS_ID, issuerBusiness.getBusinessId()).attr("storeId", storeId).log();
        return issuerStoreService.getIssuerStore(issuerBusiness.getBusinessId(), storeId).toCompletableFuture();
    }

    @GraphQLQuery(name = "issuerCounter")
    public CompletableFuture<IssuerCounter> getIssuerCounter(@GraphQLContext IssuerStore issuerStore, @GraphQLArgument(name = "counterId") Integer counterId) {
        logger.info("issuer business query for issuer store").attr(BUSINESS_ID, issuerStore.getBusinessId()).attr("storeId", issuerStore.getStoreId()).log();
        return issuerCounterService.getIssuerCounter(issuerStore.getBusinessId(), issuerStore.getStoreId(), counterId).toCompletableFuture();
    }

    @GraphQLQuery(name = "issuerTerminal")
    public CompletableFuture<IssuerTerminal> getIssuerTerminal(@GraphQLContext IssuerCounter issuerCounter, @GraphQLArgument(name = "aid") String aid, @GraphQLArgument(name = "mid") String mid, @GraphQLArgument(name = "tid") String tid) {
        logger.info("issuer business query for issuer store").attr(BUSINESS_ID, issuerCounter.getBusinessId()).attr("storeId", issuerCounter.getStoreId()).log();
        throw new CompletionException(new Throwable("failed to fetch terminal information"));
//        return issuerTerminalService.getIssuerTerminal(issuerCounter.getBusinessId(), issuerCounter.getStoreId(), issuerCounter.getCounterId(), aid, mid, tid).toCompletableFuture();
    }
}
