package com.example.atlasgraphql.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GraphQLRequest {

    private final String query;
    private final Variables variables;
    private final String operationName;
}
