package com.example.atlasgraphql.model;

import lombok.Builder;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Builder
public class Variables {

    private final Long tenantId;
    private final Long businessId;
    private final Integer storeId;
    private final Integer counterId;

    public Map<String, Object> mapper() {
        Map<String, Object> variableMap = new HashMap<>();
        variableMap.put("tenantId", tenantId);
        variableMap.put("businessId", businessId);
        variableMap.put("storeId", storeId);
        variableMap.put("counterId", counterId);
        return variableMap;
    }
}
