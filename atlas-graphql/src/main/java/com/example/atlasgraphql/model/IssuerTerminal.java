package com.example.atlasgraphql.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Builder;

@Builder
public class IssuerTerminal {

    private final Long businessId;
    private final Integer storeId;
    private final Integer counterId;
    private final String aid;
    private final String mid;
    private final String tid;
    private final String currency;
    private final String type;

    @GraphQLQuery
    public Long getBusinessId() {
        return businessId;
    }

    @GraphQLQuery
    public Integer getStoreId() {
        return storeId;
    }

    @GraphQLQuery
    public Integer getCounterId() {
        return counterId;
    }

    @GraphQLQuery
    public String getAid() {
        return aid;
    }

    @GraphQLQuery
    public String getMid() {
        return mid;
    }

    @GraphQLQuery
    public String getTid() {
        return tid;
    }

    @GraphQLQuery
    public String getCurrency() {
        return currency;
    }

    @GraphQLQuery
    public String getType() {
        return type;
    }
}
