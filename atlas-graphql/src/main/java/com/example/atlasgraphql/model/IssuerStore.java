package com.example.atlasgraphql.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Builder;

@Builder
public class IssuerStore {

    private Long businessId;
    private Integer storeId;
    private String storeName;

    @GraphQLQuery(name = "businessId")
    public Long getBusinessId() {
        return businessId;
    }

    @GraphQLQuery(name = "storeId")
    public Integer getStoreId() {
        return storeId;
    }

    @GraphQLQuery(name = "storeName")
    public String getStoreName() {
        return storeName;
    }
}
