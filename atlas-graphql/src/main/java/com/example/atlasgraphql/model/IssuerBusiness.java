package com.example.atlasgraphql.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Builder;

@Builder
public class IssuerBusiness {

    private String businessName;
    private Long businessId;
    private Long tenantId;

    @GraphQLQuery(name = "businessName")
    public String getBusinessName() {
        return businessName;
    }

    @GraphQLQuery(name = "businessId")
    public Long getBusinessId() {
        return businessId;
    }

    @GraphQLQuery(name = "tenantId")
    public Long getTenantId() {
        return tenantId;
    }
}
