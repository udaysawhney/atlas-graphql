package com.example.atlasgraphql.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Builder;

@Builder
public class Vector {

    private final Long tenantId;
    private final String entityName;
    private final String entityValue;
    private final String vectorName;
    private final String vectorValue;

    @GraphQLQuery(name = "tenantId")
    public Long getTenantId() {
        return tenantId;
    }

    @GraphQLQuery(name = "entityName")
    public String getEntityName() {
        return entityName;
    }

    @GraphQLQuery(name = "entityValue")
    public String getEntityValue() {
        return entityValue;
    }

    @GraphQLQuery(name = "vectorName")
    public String getVectorName() {
        return vectorName;
    }

    @GraphQLQuery(name = "vectorValue")
    public String getVectorValue() {
        return vectorValue;
    }
}
