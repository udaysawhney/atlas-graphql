package com.example.atlasgraphql.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Builder;

import java.util.Map;

@Builder
public class IssuerCounter {

    private final Long businessId;
    private final Integer storeId;
    private final Integer counterId;
    private final Map<String, String> ifiToLedgerMap;

    @GraphQLQuery(name = "businessId")
    public Long getBusinessId() {
        return businessId;
    }

    @GraphQLQuery(name = "storeId")
    public Integer getStoreId() {
        return storeId;
    }

    @GraphQLQuery(name = "counterId")
    public Integer getCounterId() {
        return counterId;
    }

    @GraphQLQuery(name = "ifiToLedgerMap")
    public Map<String, String> getIfiToLedgerMap() {
        return ifiToLedgerMap;
    }
}
