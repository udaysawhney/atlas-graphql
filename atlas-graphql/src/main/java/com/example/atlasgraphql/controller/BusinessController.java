package com.example.atlasgraphql.controller;

import com.example.atlasgraphql.model.GraphQLRequest;
import com.example.atlasgraphql.model.Variables;
import com.example.atlasgraphql.query.IssuerBusinessQuery;
import com.google.gson.Gson;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.DataFetcherResult;
import graphql.schema.GraphQLSchema;
import in.zeta.spectra.capture.SpectraLogger;
import io.leangen.graphql.GraphQLSchemaGenerator;
import olympus.trace.OlympusSpectra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

@RestController
public class BusinessController {

    private final SpectraLogger logger = OlympusSpectra.getLogger(BusinessController.class);
    private final GraphQL graphQL;

    @Autowired
    public BusinessController(IssuerBusinessQuery issuerBusinessQuery) {
        GraphQLSchema schema = new GraphQLSchemaGenerator()
                .withBasePackages("com.example.atlasgraphql")
                .withOperationsFromSingleton(issuerBusinessQuery, IssuerBusinessQuery.class)
                .generate();
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    @PostMapping(value = "/tenant/{tenantId}/business/{businessId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CompletableFuture<Map<String, Object>> businessResource(@RequestBody GraphQLRequest request,
                                                                  @PathVariable("tenantId") Long tenantId,
                                                                  @PathVariable("businessId") Long businessId,
                                                                  HttpServletRequest raw) {

        logger.info("params")
                .attr("tenantId", tenantId)
                .attr("businessId", businessId)
                .fill("variables", request.getVariables()).log();

        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(request.getQuery())
                .variables(request.getVariables().mapper())
                .context(raw)
                .build();

        return graphQL.executeAsync(executionInput).thenApply(this::getStringObjectMap);
    }

    private Map<String, Object> getStringObjectMap(ExecutionResult executionResult) {
        Map<String, Object> result = new HashMap<>();
        result.put("data", executionResult.getData());
        List<Map<String, Object>> errors = executionResult.getErrors()
                .stream()
                .map(error -> {
                    Map<String, Object> errorMap = new HashMap<>();
                    errorMap.put("errorMessage", error.getMessage());
                    errorMap.put("errorCode", 500); // get the code somehow from the error object

                    return errorMap;
                })
                .collect(toList());
        result.put("errors", errors);
        return result;
    }
}
