package com.example.atlasgraphql.dao;

import com.example.atlasgraphql.model.IssuerBusiness;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Repository
public class IssuerBusinessRepository {

    private static final List<IssuerBusiness> issuerBusinesses = Arrays.asList(
            IssuerBusiness.builder().businessId(1L).businessName("B-1").tenantId(1L).build(),
            IssuerBusiness.builder().businessId(2L).businessName("B-2").tenantId(1L).build(),
            IssuerBusiness.builder().businessId(3L).businessName("B-3").tenantId(1L).build()
    );

    public CompletionStage<IssuerBusiness> getIssuerBusiness(Long businessId, Long tenantId) {
        return CompletableFuture.supplyAsync(() -> issuerBusinesses.stream().filter(issuerBusiness -> issuerBusiness.getBusinessId().equals(businessId) && issuerBusiness.getTenantId().equals(tenantId)).findFirst().orElse(null));
    }
}
